//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2020-08-05
//
//------------------------------------------------------------------------------

#include <QDesktopServices>

#include <core.tpp>
#include <SApplication.hpp>

#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent}
{
	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	m_settings.restoreState(this);
	processSplitter->restoreState(m_settings.value(ProcessSplitter).toByteArray());
	m_fileSystem.setFilter(QDir::AllDirs | QDir::AllEntries |QDir::NoDotAndDotDot);
	m_fileSystem.setNameFilters({"*.xml"});
	treeView->setModel(&m_fileSystem);
	treeView->hideColumn(1);
	treeView->hideColumn(2);
	treeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
	treeView->sortByColumn(0, Qt::AscendingOrder);
	connectActions();
	connectWidgets();
	rootPathLineEdit->setText(m_settings.value(RootPath).toString());
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(SApplication::Theme, QIcon::themeName());
	m_settings.setValue(ProcessSplitter, processSplitter->saveState());
	m_settings.setValue(RootPath, rootPathLineEdit->text());
}

void MainWindow::connectActions()
{
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
}

void MainWindow::connectWidgets()
{
	connect(rootFolderPushButton, &QPushButton::clicked, [this]
	{
		rootPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(rootPathLineEdit, &QLineEdit::textChanged, [this](QString const &text)
	{
		m_fileSystem.setRootPath(text);
		treeView->setRootIndex(m_fileSystem.index(text));
		treeView->resizeColumnToContents(1);
	});
	connect(treeView->selectionModel(), &QItemSelectionModel::selectionChanged, [this]
	{
		extractorWidget->setDisabled(treeView->selectionModel()->selectedIndexes().isEmpty());
	});
	connect(treeView, &QTreeView::doubleClicked, [this](QModelIndex const &index)
	{
		QDesktopServices::openUrl(QUrl::fromLocalFile(m_fileSystem.fileInfo(index).absoluteFilePath()));
	});
	connect(extractorPushButton, &QPushButton::clicked, [this]
	{
		extractorWidget->extract(selectedFiles());
	});
}

QFileInfoList MainWindow::selectedFiles() const
{
	QFileInfoList files;

	for (auto const &index: treeView->selectionModel()->selectedIndexes())
		if (index.column() == 0)
			files << File::getFiles(m_fileSystem.fileInfo(index), {"*.xml"}, true);
	return files;
}
