//------------------------------------------------------------------------------
//
// Extractor.cpp created by Yyhrs 2020-08-05
//
//------------------------------------------------------------------------------

#include <QRegularExpression>

#include "Extractor.hpp"

Extractor::TagFileUnitValues Extractor::extract(QFileInfo const &resource)
{
	QFile             file{resource.absoluteFilePath()};
	TagFileUnitValues storage;

	if (file.open(QFile::ReadOnly))
	{
		QString          currentUnit;
		QXmlStreamReader xml;

		xml.clear();
		xml.setDevice(&file);
		while (!xml.hasError() && !xml.isEndDocument())
		{
			if (xml.isStartElement())
			{
				if (xml.attributes().hasAttribute(s_name))
				{
					currentUnit = xml.attributes().value(s_name).toString();
					storage["@" + s_name][resource.absoluteFilePath()][currentUnit] << currentUnit;
				}
				else if (!currentUnit.isEmpty())
					storage[xml.name().toString()][resource.absoluteFilePath()][currentUnit] << xml.readElementText(QXmlStreamReader::IncludeChildElements);
			}
			xml.readNext();
		}
		file.close();
	}
	else
		qWarning() << file.errorString();
	return storage;
}

void Extractor::reconstruct(QFileInfo const &resource, Extractor::UnitTagValues storage, bool createMissing)
{
	QFile        file{resource.absoluteFilePath()};
	QDomDocument document;

	if (!file.open(QFile::ReadOnly))
		return ;
	if (document.setContent(&file))
	{
		QDomElement root{document.documentElement()};
		QDomNode    firstChild{root.firstChild()};

		file.close();
		while (!firstChild.isNull())
		{
			QDomElement firstElement{firstChild.toElement()};

			if (!firstElement.isNull())
				if (storage.contains(firstElement.attribute(s_name)))
				{
					QDomNode secondChild{firstChild.firstChild()};
					auto     tags{storage[firstElement.attribute(s_name)].keys()};

					if (tags.contains("@" + s_name))
					{
						firstElement.setAttribute(s_name, storage[firstElement.attribute(s_name)]["@" + s_name].takeFirst().toString());
						tags.removeOne("@" + s_name);
					}
					while (!secondChild.isNull())
					{
						QDomElement secondElement{secondChild.toElement()};

						if (storage[firstElement.attribute(s_name)].contains(secondElement.tagName()))
						{
							QDomText newNodeText{document.createTextNode(storage[firstElement.attribute(s_name)][secondElement.tagName()].takeFirst().toString())};

							tags.removeOne(secondElement.tagName());
							secondElement.replaceChild(newNodeText, secondElement.firstChild());
						}
						secondChild = secondChild.nextSibling();
					}
					if (createMissing)
						for (auto const &tag: tags)
							while (!storage[firstElement.attribute(s_name)][tag].isEmpty())
							{
								QDomElement newNodeTag{document.createElement(tag)};
								QDomText    newNodeText{document.createTextNode(storage[firstElement.attribute(s_name)][tag].takeFirst().toString())};

								newNodeTag.appendChild(newNodeText);
								firstChild.appendChild(newNodeTag);
							}
				}
			firstChild = firstChild.nextSibling();
		}
		if (file.open(QIODevice::Truncate | QIODevice::WriteOnly))
		{
			QString     content;
			QTextStream stream{&content};

			document.save(stream, 4, QDomNode::EncodingFromDocument);
			content.replace("&#xd;", "");
			file.write(content.toUtf8());
		}
	}
	file.close();
}

void Extractor::inject(QFileInfo const &resource, UnitTagValues storage)
{
	QFile   file{resource.absoluteFilePath()};
	QString data;

	if (!file.open(QFile::ReadOnly))
		return ;
	data = file.readAll();
	file.close();
	if (!file.open(QIODevice::Truncate | QIODevice::WriteOnly))
		return ;
	for (auto const &unit: storage.keys())
		for (auto const &tag: storage[unit].keys())
			if (!tag.contains("@" + s_name))
			{
				auto unitPattern{QString{R"((<.*?Name="%1".*?>)"}.arg(unit)};

				for (auto const &value: storage[unit][tag])
				{
					auto               tagPattern{QString{R"(.*?<%1.*?>).*?(<\/%1>))"}.arg(tag)};
					QRegularExpression unitRegex{unitPattern + tagPattern, QRegularExpression::DotMatchesEverythingOption};

					data.replace(unitRegex, QString{R"(\1%1\2)"}.arg(value.toString()));
					unitPattern += QString{R"(.*?<%1.*?>.*?<\/%1>)"}.arg(tag);
				}
			}
	for (auto const &unit: storage.keys())
		for (auto const &tag: storage[unit].keys())
			if (tag.contains("@" + s_name))
			{
				QString            pattern{R"((<.*?Name=")%1(".*?>))"};
				QRegularExpression unitRegex{pattern.arg(unit)};

				data.replace(unitRegex, QString{R"(\1%1\2)"}.arg(storage[unit][tag].value(0).toString()));
			}
	file.write(data.toUtf8());
	file.close();
}
