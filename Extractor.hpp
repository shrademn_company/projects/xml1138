//------------------------------------------------------------------------------
//
// Extractor.hpp created by Yyhrs 2020-08-05
//
//------------------------------------------------------------------------------

#ifndef EXTRACTOR_HPP
#define EXTRACTOR_HPP

#include <QDebug>
#include <QDomDocument>
#include <QFileInfo>
#include <QMap>
#include <QXmlStreamReader>

class Extractor
{
public:
	using TagFileUnitValues = QMap<QString, QMap<QString, QMap<QString, QVariantList>>>;
	using UnitTagValues = QMap<QString, QMap<QString, QVariantList>>;

	Extractor() = default;
	~Extractor() = default;

	TagFileUnitValues extract(QFileInfo const &resource);
	void              reconstruct(QFileInfo const &resource, UnitTagValues storage, bool createMissing);
	void              inject(QFileInfo const &resource, UnitTagValues storage);

	static inline QString const s_name{QStringLiteral("Name")};
};

inline QDebug operator<<(QDebug debug, const QMap<QString, QMap<QString, QMap<QString, QVariantList>>> &storage)
{
	for (auto const &first: storage.keys())
		for (auto const &second: storage[first].keys())
			for (auto const &third: storage[first][second].keys())
				qDebug() << first << second << third << storage[first][second][third];
	return debug.maybeSpace();
}

#endif // EXTRACTOR_HPP
