#-------------------------------------------------
#
# Project created by Shrademn
#
#-------------------------------------------------

QT += core gui widgets xml concurrent
TEMPLATE = app
CONFIG += c++17
DEFINES += QT_IMPLICIT_QFILEINFO_CONSTRUCTION

DESTDIR = $$PWD/build
TARGET = XML1138

VERSION = 1.0.0.0
RC_ICONS = $${TARGET}.ico
QMAKE_TARGET_COMPANY = "Shrademn'Company"
QMAKE_TARGET_PRODUCT = "✭"
QMAKE_TARGET_DESCRIPTION = $${TARGET}
QMAKE_TARGET_COPYRIGHT = "☭"

SOURCES += main.cpp \
	ExtractorWidget.cpp \
	MainWindow.cpp \
	Extractor.cpp

HEADERS += \
	ExtractorWidget.hpp \
	MainWindow.hpp \
	Extractor.hpp


FORMS += \
    ExtractorWidget.ui \
    MainWindow.ui

RESOURCES += resources.qrc

include(core/Core.pri)
include(widget/WidgetEnhanced.pri)
include(widget/WidgetMeta.pri)
include(xlsx/QXlsx.pri)
