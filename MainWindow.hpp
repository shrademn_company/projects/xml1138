//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2020-08-05
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QFileSystemModel>

#include <Settings.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum Page
	{
		Extractor,
		Validator
	};
	enum SettingsKey
	{
		RootPath,
		ProcessSplitter
	};
	Q_ENUM(SettingsKey)

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	void connectActions();
	void connectWidgets();

	QFileInfoList selectedFiles() const;

	Settings         m_settings;
	QFileSystemModel m_fileSystem;
};

#endif // MAINWINDOW_HPP
