//------------------------------------------------------------------------------
//
// ExtractorWidget.cpp created by Yyhrs 2021/03/07
//
//------------------------------------------------------------------------------

#include <QDesktopServices>
#include <QFileDialog>
#include <QInputDialog>

#include <MessageDialog.hpp>
#include <xlsxdocument.h>

#include "ExtractorWidget.hpp"

ExtractorWidget::ExtractorWidget(QWidget *parent):
	QWidget{parent},
	m_00104492v{":/#001044.92v.gif"}
{
	setupUi(this);
	m_tagsProxy.setSourceModel(&m_tags);
	m_tagsProxy.setRecursiveFilteringEnabled(true);
	m_tagsProxy.setFilterCaseSensitivity(Qt::CaseInsensitive);
	tagTreeView->setModel(&m_tagsProxy);
	m_unitsProxy.setFilterCaseSensitivity(Qt::CaseInsensitive);
	m_unitsProxy.setSourceModel(&m_units);
	unitListView->setModel(&m_unitsProxy);
	m_columnsProxy.setFilterCaseSensitivity(Qt::CaseInsensitive);
	m_columnsProxy.setSourceModel(&m_columns);
	columnListView->setModel(&m_columnsProxy);
	label->setMovie(&m_00104492v);
	label->hide();
	connectActions();
	connectWidgets();
	connect(&m_00104492v, &QMovie::frameChanged, [this](int frameNumber)
	{
		if (frameNumber == m_00104492v.frameCount() - 1)
		{
			m_00104492v.stop();
			label->hide();
		}
	});
	partsSplitter->restoreState(m_settings.value(PartsSplitter).toByteArray());
	exportPathLineEdit->setText(m_settings.value(ExportPath).toString());
	injectCheckBox->setChecked(m_settings.value(Inject, false).toBool());
}

ExtractorWidget::~ExtractorWidget()
{
	m_settings.setValue(PartsSplitter, partsSplitter->saveState());
	m_settings.setValue(ExportPath, exportPathLineEdit->text());
	m_settings.setValue(Inject, injectCheckBox->isChecked());
}

void ExtractorWidget::extract(QFileInfoList const &files)
{
	m_storage.clear();
	for (auto const &file: files)
	{
		auto storage{m_extractor.extract(file)};

		for (auto const &tag: storage.keys())
			m_storage[tag].insert(storage[tag]);
	}
	fillTags();
}

void ExtractorWidget::connectActions()
{
	tagTreeView->addActions({actionGroup, actionClearGroup});
	connect(actionGroup, &QAction::triggered, [this]
	{
		auto        text{QInputDialog::getText(this, {}, "Group Name", QLineEdit::Normal, {}, nullptr, Qt::Dialog | Qt::FramelessWindowHint)};
		QStringList tags;

		if (text.isEmpty())
			return ;
		m_settings.beginGroup(Groups);
		tags = m_settings.value(text).toStringList();

		for (auto const &index: tagTreeView->selectionModel()->selectedIndexes())
		{
			for (auto const &key: m_settings.allKeys())
			{
				auto values{m_settings.value(key).toStringList()};

				values.removeAll(index.data().toString());
				m_settings.setValue(key, values);
			}
			tags << index.data().toString();
		}
		m_settings.setValue(text, tags);
		m_settings.endGroup();
		fillTags();
	});
	connect(actionClearGroup, &QAction::triggered, [this]
	{
		QStringList tags;

		m_settings.beginGroup(Groups);
		for (auto const &key: m_settings.allKeys())
		{
			auto values{m_settings.value(key).toStringList()};

			for (auto const &index: tagTreeView->selectionModel()->selectedIndexes())
				values.removeAll(index.data().toString());
			m_settings.setValue(key, values);
		}
		m_settings.endGroup();
		fillTags();
	});
}

void ExtractorWidget::connectWidgets()
{
	connect(tagsFilterLineEdit, &QLineEdit::textChanged, &m_tagsProxy, &QSortFilterProxyModel::setFilterFixedString);
	connect(tagTreeView->selectionModel(), &QItemSelectionModel::selectionChanged, [this]
	{
		actionGroup->setDisabled(tagTreeView->selectionModel()->selectedIndexes().isEmpty());
		actionClearGroup->setDisabled(tagTreeView->selectionModel()->selectedIndexes().isEmpty());
		exportPushButton->setDisabled(tagTreeView->selectionModel()->selectedIndexes().isEmpty());
	});
	connect(exportPushButton, &QPushButton::clicked, this, &ExtractorWidget::exportFile);
	connect(exportFilePushButton, &QPushButton::clicked, [this]
	{
		exportPathLineEdit->openBrowser(PathLineEdit::File, "Export File (*xlsx)");
	});
	connect(openFilePushButton, &QPushButton::clicked, [this]
	{
		QDesktopServices::openUrl(QUrl::fromLocalFile(exportPathLineEdit->text()));
	});
	connect(exportPathLineEdit, &QLineEdit::textChanged, this, &ExtractorWidget::importFile);
	connect(unitsFilterLineEdit, &QLineEdit::textChanged, &m_unitsProxy, &QSortFilterProxyModel::setFilterFixedString);
	connect(unitListView->selectionModel(), &QItemSelectionModel::selectionChanged, [this]
	{
		importPushButton->setDisabled(unitListView->selectionModel()->selectedIndexes().isEmpty() || columnListView->selectionModel()->selectedIndexes().isEmpty());
	});
	connect(columnsFilterLineEdit, &QLineEdit::textChanged, &m_columnsProxy, &QSortFilterProxyModel::setFilterFixedString);
	connect(columnListView->selectionModel(), &QItemSelectionModel::selectionChanged, [this]
	{
		importPushButton->setDisabled(unitListView->selectionModel()->selectedIndexes().isEmpty() || columnListView->selectionModel()->selectedIndexes().isEmpty());
	});
	connect(importPushButton, &QPushButton::clicked, this, &ExtractorWidget::import);
	connect(createMissingTagsCheckBox, &QCheckBox::toggled, [this](bool checked)
	{
		if (checked)
			injectCheckBox->setChecked(false);
	});
	connect(injectCheckBox, &QCheckBox::toggled, [this](bool checked)
	{
		if (checked)
			createMissingTagsCheckBox->setChecked(false);
	});
}

void ExtractorWidget::fillTags()
{
	QMap<QStringList, QStandardItem *> groups;
	auto                               *othersGroup{new QStandardItem{"Others"}};

	othersGroup->setSelectable(false);
	m_tags.clear();
	m_tags.setHorizontalHeaderItem(0, new QStandardItem{"Tags"});
	m_settings.beginGroup(Groups);
	for (auto const &key: m_settings.allKeys())
	{
		auto values{m_settings.value(key).toStringList()};

		if (values.isEmpty())
			m_settings.remove(key);
		else
		{
			auto *group{new QStandardItem{key}};

			group->setSelectable(false);
			groups[values] = group;
		}
	}
	m_settings.endGroup();
	for (auto const &tag: m_storage.keys())
	{
		bool others{true};

		for (auto const &group: groups.keys())
			if (group.contains(tag))
			{
				groups[group]->appendRow(new QStandardItem{tag});
				others = false;
			}
		if (others)
			othersGroup->appendRow(new QStandardItem{tag});
	}
	for (auto const &group: qAsConst(groups))
		m_tags.appendRow(group);
	tagTreeView->expandAll();
	m_tags.appendRow(othersGroup);
	if (groups.isEmpty())
		tagTreeView->expandAll();
}

void ExtractorWidget::exportFile()
{
	QFileInfo file{QFileDialog::getSaveFileName(this, "Export", exportPathLineEdit->text(), "XLSX values (*.xlsx)")};

	if (!file.exists() || QFile::remove(file.absoluteFilePath()))
	{
		QXlsx::Document                                   xlsx{file.absoluteFilePath()};
		QMap<QPair<QString, QString>, QMap<int, QString>> items;
		int                                               row{2};
		int                                               column{3};
		QXlsx::Format                                     format;

		format.setFontBold(true);
		format.setPatternBackgroundColor(Qt::lightGray);
		format.setBorderColor(Qt::black);
		format.setBottomBorderStyle(QXlsx::Format::BorderThin);
		xlsx.write(1, 1, "Path", format);
		xlsx.write(1, 2, "Unit", format);
		xlsx.setColumnWidth(2, 32);
		for (auto const &index: tagTreeView->selectionModel()->selectedIndexes())
		{
			auto const tag{index.data().toString()};
			int        remaining{1};
			int        counter{0};

			while (remaining--)
			{
				xlsx.write(1, column, tag, format);
				for (auto const &file: m_storage[tag].keys())
					for (auto const &name: m_storage[tag][file].keys())
					{
						if (counter < m_storage[tag][file][name].count())
							items[{file, name}][column] = m_storage[tag][file][name][counter].toString();
						remaining = qMax(m_storage[tag][file][name].count() - counter - 1, remaining);
					}
				xlsx.setColumnWidth(column, 16);
				++counter;
				++column;
			}
		}
		format.setBottomBorderColor(Qt::gray);
		format.setRightBorderStyle(QXlsx::Format::BorderThin);
		for (auto const &key: items.keys())
		{
			xlsx.write(row, 1, key.first);
			xlsx.write(row, 2, key.second, format);
			for (auto const &value: items[key].keys())
				xlsx.write(row, value, items[key][value]);
			++row;
		}
		xlsx.save();
		exportPathLineEdit->setText(file.absoluteFilePath());
		importFile(file.absoluteFilePath());
	}
	else
		MessageDialog{MessageDialog::Warning, "Cannot replace file."};
}

void ExtractorWidget::importFile(QString const &text)
{
	if (QFileInfo::exists(text))
	{
		QXlsx::Document xlsx{text};
		QStringList     units;
		QStringList     columns;

		for (int row = 2; row <= xlsx.dimension().lastRow(); ++row)
			units << xlsx.readValue(row, 2).toString();
		m_units.setStringList(units);
		for (int column = 3; column <= xlsx.dimension().lastColumn(); ++column)
			columns << xlsx.readValue(1, column).toString();
		columns.removeDuplicates();
		m_columns.setStringList(columns);
	}
}

void ExtractorWidget::import()
{
	QXlsx::Document                         xlsx{exportPathLineEdit->text()};
	QStringList                             names;
	QStringList                             tags;
	QMap<QString, Extractor::UnitTagValues> storage;

	for (auto const &index: unitListView->selectionModel()->selectedIndexes())
		names << index.data().toString();
	for (auto const &index: columnListView->selectionModel()->selectedIndexes())
		tags << index.data().toString();
	storage.clear();
	for (int column = 3; column <= xlsx.dimension().lastColumn(); ++column)
	{
		auto const tag{xlsx.readValue(1, column).toString()};

		for (int row = 2; row <= xlsx.dimension().lastRow(); ++row)
		{
			auto const file{xlsx.readValue(row, 1).toString()};
			auto const name{xlsx.readValue(row, 2).toString()};

			if (names.contains(name) && tags.contains(tag))
				storage[file][name][tag] << xlsx.readValue(row, column);
		}
	}
	for (auto const &file: storage.keys())
	{
		if (injectCheckBox->isChecked())
			m_extractor.inject(file, storage[file]);
		else
			m_extractor.reconstruct(file, storage[file], createMissingTagsCheckBox->isChecked());
	}
	m_00104492v.start();
	label->show();
}
