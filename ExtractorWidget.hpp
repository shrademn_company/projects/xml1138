//------------------------------------------------------------------------------
//
// ExtractorWidget.hpp created by Yyhrs 2021/03/07
//
//------------------------------------------------------------------------------

#ifndef EXTRACTORWIDGET_HPP
#define EXTRACTORWIDGET_HPP

#include <QMovie>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QStringListModel>

#include <Settings.hpp>

#include "Extractor.hpp"

#include "ui_ExtractorWidget.h"

class ExtractorWidget: public QWidget, private Ui::ExtractorWidget
{
	Q_OBJECT

public:
	enum SettingsKey
	{
		SchemataPath,
		ExportPath,
		PartsSplitter,
		Groups,
		Inject
	};
	Q_ENUM(SettingsKey)

	explicit ExtractorWidget(QWidget *parent = nullptr);
	~ExtractorWidget() override;

	void extract(QFileInfoList const &files);

private:
	void connectActions();
	void connectWidgets();

	void fillTags();
	void exportFile();
	void importFile(QString const &text);
	void import();

	Settings                     m_settings;
	QMovie                       m_00104492v;
	QStandardItemModel           m_tags;
	QSortFilterProxyModel        m_tagsProxy;
	QStringListModel             m_units;
	QSortFilterProxyModel        m_unitsProxy;
	QStringListModel             m_columns;
	QSortFilterProxyModel        m_columnsProxy;
	Extractor                    m_extractor;
	Extractor::TagFileUnitValues m_storage;
};

#endif // EXTRACTORWIDGET_HPP
